//
//  appkaApp.swift
//  appka
//
//  Created by Даниил Тимонин on 28.09.2022.
//

import SwiftUI

@main
struct appkaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
